package org.monitoring.log.monitor;

import org.monitoring.log.LogEntry;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.monitoring.appender.ConsoleAppender.*;

/**
 * Monitors events regarding a threshold within a time window.
 */
public class TimeWindowThresholdMonitor<T> extends Monitor<T> {
    private final long threshold;
    private final TimeWindow timeWindow;
    private int totalEvents = 0;
    private boolean belowThreshold = true;

    /**
     * TimeWindowThresholdEventMonitor constructor.
     * @param timeWindow time window to keep track of, in seconds.
     * @param threshold number of average requests in the time window that triggers an alert.
     */
    public TimeWindowThresholdMonitor(int timeWindow, int threshold) {
        this.threshold = threshold;
        this.timeWindow = new TimeWindow(timeWindow);
    }

    @Override
    public void process(T event) {
        totalEvents++;
        long currentTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        timeWindow.add(currentTime);
        verifyThreshold();
    }

    public void verifyThreshold() {
        long currentTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

        totalEvents -= timeWindow.removeElapsed(currentTime);

        int average = totalEvents/timeWindow.getWindow();

        if (average >= threshold && belowThreshold) {
            setAlertMessage(ANSI_RED + "\nHigh traffic generated an alert - hits = " + totalEvents + ", triggered at " + new Date() + ANSI_RESET);
            belowThreshold = false;
        }

        if (average < threshold && !belowThreshold) {
            setAlertMessage(ANSI_GREEN + "\nTraffic recovered - hits = " + totalEvents + ", triggered at " + new Date()  + ANSI_RESET);
            belowThreshold = true;
        }
    }

    @Override
    public String get() {
        verifyThreshold();
        return hasAlert() ? getAlertMessage() : null;
    }
}
