package org.monitoring.log.monitor;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Represents a window of time bucket counters.
 */
public class TimeWindow {
    private final Deque<TimeBucketCounter> windowQueue = new LinkedList<>();
    private final int window;

    public TimeWindow(int window) {
        this.window = window;
    }

    public long removeElapsed(long currentTime) {
        long removedEvents = 0;
        while (!windowQueue.isEmpty() && (currentTime - windowQueue.peekFirst().getTimestamp() > window)) {
            removedEvents += windowQueue.removeFirst().getCount();
        }
        return removedEvents;
    }

    public void add(long currentTime) {
        if (windowQueue.isEmpty() || windowQueue.getLast().getTimestamp() != currentTime) {
            windowQueue.add(new TimeBucketCounter(currentTime));
        } else {
            windowQueue.getLast().incrementCount();
        }
    }

    public int getWindow() {
        return window;
    }

}
