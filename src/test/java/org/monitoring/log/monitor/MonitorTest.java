package org.monitoring.log.monitor;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.monitoring.log.LogEntry;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

public class MonitorTest {

    private Monitor monitor;

    @Before
    public void setUp() {
        monitor = mock(Monitor.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    public void hasAlertInitially() {
        assertThat("hasAlert() should be false initially", !monitor.hasAlert());
    }

    @Test
    public void hasAlert() {
        monitor.setAlertMessage("alert message!");
        assertThat("hasAlert() should be true after setting alert message", monitor.hasAlert());
    }

    @Test
    public void getAlertMessage() {
        String message = "alert message!";
        monitor.setAlertMessage(message);
        assertThat("getAlertMessage() should be true", monitor.getAlertMessage(), equalTo(message));
        assertThat("hasAlert() should be false after reading message", !monitor.hasAlert());
    }
}