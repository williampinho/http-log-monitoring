package org.monitoring.receiver;

import org.monitoring.receiver.publisher.Publisher;

/**
 * Listen to changes on some resource.
 * Requires a publisher to publish changes received.
 */
public abstract class Receiver<T> implements Runnable {
    protected final Publisher<T> publisher;

    public Receiver(Publisher<T> publisher) {
        if (publisher == null) {
            throw new IllegalStateException("publisher cannot be null");
        }
        this.publisher = publisher;
    }
}
