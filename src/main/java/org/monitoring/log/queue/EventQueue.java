package org.monitoring.log.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * General event queue
 * @param <T>
 */
public abstract class EventQueue<T> {
    private final BlockingQueue<T> queue;

    protected EventQueue() {
        queue = new LinkedBlockingQueue<>();
    }

    public T poll() {
        return queue.poll();
    }

    public void add(T event) {
        queue.offer(event);
    }
}
