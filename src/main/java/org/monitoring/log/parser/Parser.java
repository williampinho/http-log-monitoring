package org.monitoring.log.parser;

import org.monitoring.log.LogEntry;

import java.util.IllegalFormatException;

/**
 * Parses a line of input.
 */
public interface Parser<T> {
    T parse(String input) throws IllegalFormatException;
}
