package org.monitoring.log.monitor;

import org.monitoring.log.LogEntry;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

public class TimeWindowThresholdMonitorTest {

    private TimeWindowThresholdMonitor<LogEntry> monitor;
    private LogEntry mockLogEntry = mock(LogEntry.class);

    @Test
    public void processNoEvents() {
        monitor = new TimeWindowThresholdMonitor<>(10, 1);
        monitor.verifyThreshold();
        assertThat("no processed events should not trigger an alert", !monitor.hasAlert());
    }

    @Test
    public void process1EventInTimeWindowOf10SecondsOfThreshold1() {
        monitor = new TimeWindowThresholdMonitor<>(10, 1);
        monitor.process(mockLogEntry);
        assertThat("1 processed org.monitoring.event, 10 sec time window, 1 threshold should not trigger an alert", !monitor.hasAlert());
    }

    @Test
    public void process9EventsInTimeWindowOf10SecondsOfThreshold1() {
        monitor = new TimeWindowThresholdMonitor<>(10, 1);
        IntStream.range(0, 9).forEach(i -> monitor.process(mockLogEntry));
        assertThat("9 processed events, 10 sec time window, 1 threshold should not trigger an alert", !monitor.hasAlert());
        assertThat("should not contain an alert message", monitor.getAlertMessage().isEmpty());
    }

    @Test
    public void process10EventsInTimeWindowOf10SecondsOfThreshold1() {
        monitor = new TimeWindowThresholdMonitor<>(10, 1);
        IntStream.range(0, 10).forEach(i -> monitor.process(mockLogEntry));
        assertThat("10 processed events, 10 sec time window, 1 threshold should trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("High traffic generated an alert"));
    }

    @Test
    public void process1EventInTimeWindowOf5SecondsOfThreshold2() {
        monitor = new TimeWindowThresholdMonitor<>(5, 2);
        IntStream.range(0, 1).forEach(i -> monitor.process(mockLogEntry));
        assertThat("1 processed org.monitoring.event, 5 sec time window, 2 threshold should not trigger an alert", !monitor.hasAlert());
        assertThat("should not contain alert message", monitor.getAlertMessage().isEmpty());
    }

    @Test
    public void process9EventsInTimeWindowOf5SecondsOfThreshold2() {
        monitor = new TimeWindowThresholdMonitor<>(5, 2);
        IntStream.range(0, 9).forEach(i -> monitor.process(mockLogEntry));
        assertThat("9 processed events, 5 sec time window, 2 threshold should trigger an alert", !monitor.hasAlert());
        assertThat("should not contain an alert message", monitor.getAlertMessage().isEmpty());
    }

    @Test
    public void process10EventsInTimeWindowOf5SecondsOfThreshold2() {
        monitor = new TimeWindowThresholdMonitor<>(5, 2);
        IntStream.range(0, 10).forEach(i -> monitor.process(mockLogEntry));
        assertThat("10 processed events, 5 sec time window, 2 threshold should trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("High traffic generated an alert"));
    }

    @Test
    public void process2EventsPerSecondInTimeWindowOf5SecondsOfThreshold2() {
        monitor = new TimeWindowThresholdMonitor<>(5, 2);
        IntStream.range(0, 9).forEach(i -> processEvent(monitor, 500));
        assertThat("9 processed events, 5 sec time window, 2 threshold should not trigger an alert",!monitor.hasAlert());
        monitor.process(mockLogEntry);
        assertThat("10 processed events, 5 sec time window, 2 threshold should trigger an alert",monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("High traffic generated an alert"));
    }

    @Test
    public void processAlertLevelBelowThreshold() throws InterruptedException {
        monitor = new TimeWindowThresholdMonitor<>(5, 1);
        IntStream.range(0, 5).forEach(i -> processEvent(monitor, 1000)); // one org.monitoring.event per second
        assertThat("5 processed events, 5 sec time window, 1 threshold should trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("High traffic generated an alert"));
        // wait one second; alert message below threshold should be triggered,
        // because there are 4 events in 5 second window, which gives an average that is below 1.0
        Thread.sleep(1000);
        monitor.verifyThreshold();
        assertThat("going below threshold should trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("Traffic recovered"));
    }

    @Test
    public void processAlertLevelBelowThreshold2() throws InterruptedException {
        monitor = new TimeWindowThresholdMonitor<>(2, 1);
        // process 2 events per second
        // this generates 2 sec time window of 4 events evenly distributed
        IntStream.range(0, 4).forEach(i -> processEvent(monitor, 500));
        assertThat("4 processed events, 2 sec time window, 1 threshold should trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("High traffic generated an alert"));

        // wait 1.5 seconds; alert message below threshold should not be triggered yet
        Thread.sleep(1500);

        // process one more org.monitoring.event
        monitor.process(mockLogEntry);

        // alert message below threshold should not be triggered yet
        // as there are still 3 events within the 2 sec window
        // average = 1.5 events which is still higher than threshold of 1.0
        monitor.verifyThreshold();
        assertThat("still above threshold should not trigger an alert", !monitor.hasAlert());

        // wait one more second; alert message below threshold should be triggered
        // as there is only one org.monitoring.event within the window
        Thread.sleep(1000);
        monitor.verifyThreshold();

        assertThat("still above threshold should not trigger an alert", monitor.hasAlert());
        assertThat("should contain alert message", monitor.getAlertMessage(), containsString("Traffic recovered"));
    }

    private void processEvent(TimeWindowThresholdMonitor<LogEntry> monitor, long timeMillis) {
        try {
            monitor.process(mockLogEntry);
            Thread.sleep(timeMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}