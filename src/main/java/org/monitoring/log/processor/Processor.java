package org.monitoring.log.processor;

import org.monitoring.log.LogEntry;

import java.util.function.Supplier;

/**
 * Event processor interface.
 */
public interface Processor<T> extends Supplier<String> {
    void process(T event);
}
