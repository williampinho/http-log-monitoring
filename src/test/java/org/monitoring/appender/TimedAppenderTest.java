package org.monitoring.appender;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import static org.mockito.Mockito.*;

public class TimedAppenderTest {

    private TimedAppender timedAppender;
    private Appender mockAppender;
    private Supplier mockSupplier;
    private String output;

    @Before
    public void setUp() throws Exception {
        mockAppender = mock(Appender.class);
        mockSupplier = mock(Supplier.class);
        output = "test";

    }

    @Test(expected = IllegalArgumentException.class)
    public void createNegativeInterval() {
        new TimedAppender(mockAppender, -1, mockSupplier);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNilInterval() {
        new TimedAppender(mockAppender, 0, mockSupplier);
    }

    @Test
    public void appendOnce() {
        int oneSecond = 1000;
        timedAppender = new TimedAppender(mockAppender, oneSecond, () -> output);

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(timedAppender);

        try {
            // sleep more than 1 second
            Thread.sleep(1100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        verify(mockAppender, times(2)).append(output);
    }

    @Test
    public void appendTwice() {
        int oneSecond = 1000;
        timedAppender = new TimedAppender(mockAppender, oneSecond, () -> output);

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(timedAppender);

        try {
            // sleep more than 2 seconds
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        verify(mockAppender, times(3)).append(output);
    }

}