package org.monitoring.log.monitor;

import org.monitoring.log.processor.Processor;

/**
 * Monitors events regarding a threshold, generating alert message.
 */
public abstract class Monitor<T> implements Processor<T> {
    private boolean hasAlert = false;
    private String alertMessage = "";

    public abstract void process(T event);
    public abstract void verifyThreshold();

    public void setAlertMessage(String message) {
        hasAlert = true;
        alertMessage = message;
    }

    public boolean hasAlert() {
        return hasAlert;
    }

    public String getAlertMessage() {
        hasAlert = false;
        return alertMessage;
    }
}
