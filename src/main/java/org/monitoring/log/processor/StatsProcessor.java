package org.monitoring.log.processor;

import org.monitoring.log.LogEntry;
import org.monitoring.log.stats.SummaryStatistics;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Supplier;

/**
 * Processes events for statistical purposes.
 */
public class StatsProcessor<T extends LogEntry> implements Processor<T>, Supplier<String> {
    private final Map<String, LongAdder> sectionCount = new ConcurrentHashMap<>();
    private final Map<String, LongAdder> userCount = new ConcurrentHashMap<>();
    private AtomicLong totalBytes = new AtomicLong(0);
    @Override
    public void process(T logEntry) {
        sectionCount.computeIfAbsent(logEntry.getSection(), e -> new LongAdder()).increment();
        userCount.computeIfAbsent(logEntry.getUser(), e -> new LongAdder()).increment();
        totalBytes.addAndGet(logEntry.getBytes());
    }

    @Override
    public synchronized String get() {
        String result = "\n***********************************" +
                "\n*  " + new Date() + "   *" +
                "\n***********************************" +
                "\nStatistics Summary:\n";
        result += (new SummaryStatistics(sectionCount, "section")).toString();
        result += (new SummaryStatistics(userCount, "user")).toString();
        result += "\n\ttotal bytes=" + totalBytes;
        result += "\n\n***********************************";
        sectionCount.clear();
        userCount.clear();
        totalBytes = new AtomicLong(0);
        return result;
    }
}
