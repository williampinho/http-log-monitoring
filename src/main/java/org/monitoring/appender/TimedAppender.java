package org.monitoring.appender;

import java.util.function.Supplier;

/**
 * Writes every (interval) seconds whatever comes from a string supplier.
 * Delegates the writing to another appender.
 */
public final class TimedAppender implements Appender, Runnable {
    private final long interval;
    private final Appender appender;
    private final Supplier<String> outputSupplier;

    public TimedAppender(Appender appender, long interval, Supplier<String> outputSupplier) {
        if (interval <= 0) {
            throw new IllegalArgumentException("interval must be positive. interval: " + interval);
        }
        this.appender = appender;
        this.interval = interval;
        this.outputSupplier = outputSupplier;
    }

    @Override
    public void append(String input) {
        this.appender.append(input);
    }

    @Override
    public void run() {
        while (true) {
            try {
                appendResult();
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void appendResult() {
        String result = outputSupplier.get();

        if (result != null) {
            this.append(result);
        }
    }

}
