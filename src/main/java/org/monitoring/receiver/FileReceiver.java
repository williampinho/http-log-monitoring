package org.monitoring.receiver;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.monitoring.receiver.publisher.Publisher;

import java.nio.file.Path;

/**
 * Watches for new lines from a file and handles to a publisher.
 * Checks if there are new file contents every tenth of a second.
 * Only listen for new lines, discarding any pre-existent content.
 */
public class FileReceiver<T> extends Receiver<T> {
    private final Path path;
    private final long POLLING_INTERVAL = 100;

    public FileReceiver(Path path, Publisher<T> publisher) {
        super(publisher);
        this.path = path;
    }

    @Override
    public void run() {
        TailerListener tailerListener = new TailerListenerAdapter() {
            @Override
            public void handle(final String line) {
                publisher.publish(line);
            }
        };

        Tailer.create(path.toFile(), tailerListener, POLLING_INTERVAL, true);
    }
}
