package org.monitoring.log.subscriber;

import org.monitoring.log.LogEntry;
import org.monitoring.log.processor.Processor;
import org.monitoring.log.queue.EventQueue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.monitoring.log.queue.LogEntryQueue;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class LogEntrySubscriberTest {

    private final int numberEvents;
    private Subscriber<LogEntry> subscriber;
    private Processor<LogEntry> mockProcessor;
    private LogEntry mockLogEntry;
    private ExecutorService executor;
    private EventQueue<LogEntry> logEntryQueue;

    @Before
    @SuppressWarnings("unchecked")
    public void initialize() {
        logEntryQueue = LogEntryQueue.get();
        mockLogEntry = new LogEntry("/foo", "user", 1);
        mockProcessor = mock(Processor.class);
        List<Processor<LogEntry>> mockList = Collections.singletonList(mockProcessor);
        subscriber = new Subscriber<>(logEntryQueue, mockList);
        executor = Executors.newFixedThreadPool(1);

    }

    public LogEntrySubscriberTest(int numberEvents) {
        this.numberEvents = numberEvents;
    }

    @Parameterized.Parameters
    public static Collection<Integer> numberEvents() {
        return Arrays.asList(2, 100, 130, 500, 1000);
    }


    @Test
    public void run() throws InterruptedException {
        executor.submit(subscriber);

        List<LogEntry> mockListLogEntry = IntStream.range(0, numberEvents).mapToObj(i -> mockLogEntry).collect(toList());

        mockListLogEntry.forEach(e -> logEntryQueue.add(e));

        Thread.sleep(100);
        verify(mockProcessor, times(numberEvents)).process(mockLogEntry);

        subscriber.stop();
        executor.shutdownNow();
    }
}