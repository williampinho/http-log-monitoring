package org.monitoring.appender;

/**
 * Appender interface.
 */
public interface Appender {
    void append(String input);
}
