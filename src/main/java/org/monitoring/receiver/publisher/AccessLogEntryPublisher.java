package org.monitoring.receiver.publisher;


import org.monitoring.log.LogEntry;
import org.monitoring.log.parser.AccessLogEntryParser;
import org.monitoring.log.parser.Parser;
import org.monitoring.log.queue.EventQueue;

/**
 * Publishes access log entries to a queue.
 * @param <T>
 */
public class AccessLogEntryPublisher<T extends LogEntry> extends Publisher<T> {
    public AccessLogEntryPublisher(EventQueue<T> queue) {
        super((Parser<T>) new AccessLogEntryParser(), queue);
    }
}
