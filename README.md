# HTTP log monitoring console program #

You can [see the program running here](https://streamable.com/o/tk7hfq). In this demo, we override
the threshold to 1 and the time window length to 5 seconds. This is to show that when it reaches 5 hits in a 5-second window, it shows a high traffic alert. Another alert is then shown when traffic recovers.

It publishes statistics every 10 seconds about sections, users and bytes transferred.

## Assumptions

### Assumptions/specification to keep it simple and to the point:

- Maximum number of requests per second that can be written to access.log are in the order of thousands.
- Threshold is an integer number.
- Analyses only appended entries to access.log.

## System goals

### Functional requirements

- Consume an actively written-to w3c-formatted HTTP access log (default: /tmp/access.log, overridable)
- Must display stats every 10 seconds about the traffic. During those 10 seconds: 
	- shows sections of the web site with the most hits (the section of "/pages/create" is "/pages").
	- shows summary statistics on the traffic as a whole.
- Able to monitor file continuously
- Create alert whenever total traffic exceeds a certain number on average for the past 2 minutes (default threshold: 10 requests/second, overridable)
- Create alert whenever total traffic drops again below threshold for the past 2 minutes

### Non functional requirements

- Robust (handle wrong inputs effectively)
- Testable
- Readable

## Implementation

### In general lines:

- FileReceiver receives changes from access.log file and delegates to a publisher;
- Publisher publishes logEntry to a queue;
- Subscriber reads logEntry, delegates to two log entry processors:
	- StatsProcessor - accumulates log entry information. When requested, generates statistics of the log entries and flushes;
	- TimeWindowThresholdMonitor - keeps track of the number of log entries in a time window. Generates alert when number of entries is larger than threshold.
- Two TimedAppenders poll the logEntry processors above to get information:
  - First appender polls StatsEventProcessor every 10 seconds for statistics;
  - Second appender polls TimeWindowThresholdEventMonitor for alerts every 100 milliseconds.

General design can be seen below:

![Scheme](img/overview.png)

## Testing

Used Junit to do unit testing.

### Classes tested:

- TimedAppender
- Subscriber
- Monitor
- TimeWindowThresholdMonitor

## Running

Built with: Java 8, Maven 3.6.3

To build, go to the project base folder and run:

```bash
$ mvn clean package
```

From the same folder, run:

```bash
$ java -jar target/http-log-monitoring.jar
```

## Program usage

Use -h option for more information:


```bash

NAME
       http-log-monitoring - console program that monitors HTTP traffic on running machine 

SYNOPSIS
        http-log-monitoring [OPTION]...

DESCRIPTION
        Consume an actively written-to w3c-formatted HTTP access log (/tmp/access.log by default).

        Display stats every 10 seconds about the traffic during those 10 seconds
        For instance, it displays sections of the website with most hits, the ones with less hits,
        the total number of hits and the average number of hits per section hit.

        Displays an alert whenever total traffic for the past 2 minutes exceeds a certain number on average.
        Also displays an alert whenever the total traffic drops again below that value on average for the past 2 minutes.

OPTIONS
        -n              interval for printing statistics, in seconds
        -T              threshold, a value between 1 and 2^31
        -f              access log file path
        -W              alert monitoring time window length, in seconds

```