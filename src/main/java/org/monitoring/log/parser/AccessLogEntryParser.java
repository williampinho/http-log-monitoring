package org.monitoring.log.parser;

import org.monitoring.log.LogEntry;
import org.monitoring.log.parser.exception.IllegalLogFormatException;

import java.util.IllegalFormatException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses an access.log file entry.
 */
public class AccessLogEntryParser implements Parser<LogEntry> {
    final String commonLogRegex = "^(\\S+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+) (\\S+)\\s*(\\S+)?\\s*\" (\\d{3}) (\\S+)";

    final Pattern pattern = Pattern.compile(commonLogRegex, Pattern.MULTILINE);

    @Override
    public LogEntry parse(String input) throws IllegalFormatException {
        Matcher matcher = pattern.matcher(input);

        if (matcher.matches()) {
            String section = getSection(matcher);
            String user = getUser(matcher);
            long bytes = getBytes(matcher);

            return new LogEntry(section, user, bytes);
        }

        throw new IllegalLogFormatException();
    }

    private long getBytes(Matcher matcher) {
        String bytes = matcher.group(9);
        return Long.parseLong(bytes);
    }

    private String getUser(Matcher matcher) {
        return matcher.group(3);
    }

    private String getSection(Matcher matcher) {
        String resource = matcher.group(6);
        int endSectionIndex = resource.indexOf("/", 1);
        return endSectionIndex != -1 ? resource.substring(0, endSectionIndex) : resource;
    }
}
