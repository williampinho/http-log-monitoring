package org.monitoring;

import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static org.monitoring.appender.ConsoleAppender.ANSI_RED;
import static org.monitoring.appender.ConsoleAppender.ANSI_RESET;

/**
 * Handles general command line input/output on program invocation.
 */
public class CommandLineArguments {
    private static final long ONE_SECOND = 1000L;
    private boolean valid = true;
    private static final Map<String, String> parameters;
    private static final String MONITORING_WINDOW_LENGTH_PARAM = "-W";
    private static final String THRESHOLD_PARAM = "-T";
    private static final String STATISTICS_INTERVAL_LENGTH_PARAM = "-n";
    private static final String ACCESS_LOG_FILE_PATH_PARAM = "-f";
    private List<String> errorMessages = new ArrayList<>();
    static {
        Map<String, String> map = new HashMap<>();
        map.put(MONITORING_WINDOW_LENGTH_PARAM, "alert monitoring time window length, in seconds");
        map.put(THRESHOLD_PARAM, "threshold, a value between 1 and 2^31");
        map.put(STATISTICS_INTERVAL_LENGTH_PARAM, "interval for printing statistics, in seconds");
        map.put(ACCESS_LOG_FILE_PATH_PARAM, "access log file path");
        parameters = Collections.unmodifiableMap(map);
    }

    private String path = "/tmp/access.log";
    private int interval = 10;
    private int threshold = 10;
    private Integer timeWindow = 120;

    public CommandLineArguments(String[] args) {
        parse(args);
    }

    private void parse(String[] args) {
        if (args.length % 2 != 0 || Arrays.asList(args).contains("-h")) {
            valid = false;
            return;
        }

        for (int i = 0; i < args.length && valid; i += 2) {
            if (isValidParameter(args[i])) {
                String parameterName = args[i];
                String value = args[i+1];

                switch (parameterName) {
                    case MONITORING_WINDOW_LENGTH_PARAM:
                        setTimeWindow(value);
                        break;
                    case THRESHOLD_PARAM:
                        setThreshold(value);
                        break;
                    case STATISTICS_INTERVAL_LENGTH_PARAM:
                        setInterval(value);
                        break;
                    case ACCESS_LOG_FILE_PATH_PARAM:
                        setAccessLogFilePath(value);
                        break;
                    default:
                }
            }
        }

        if (!Files.exists(new File(path).toPath())) {
            errorMessages.add("File " + path + " does not exist");
            valid = false;
        }
    }

    private void setAccessLogFilePath(String path) {
        this.path = path;
    }

    private void setInterval(String interval) {
        this.interval = parseInt(interval).orElse(-1);
        if (this.interval <= 0) {
            errorMessages.add("Interval must be a positive integer: " + interval);
            valid = false;
        }
    }

    private void setThreshold(String threshold) {
        this.threshold = parseInt(threshold).orElse(-1);
        if (this.threshold <= 0) {
            errorMessages.add("Threshold must be a positive integer: " + threshold);
            valid = false;
        }
    }

    private Optional<Integer> parseInt(String value) {
        try {
            return Optional.of(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    private void setTimeWindow(String timeWindow) {
        this.timeWindow = parseInt(timeWindow).orElse(-1);
        if (this.timeWindow <= 0) {
            errorMessages.add("Time window must be a positive integer: " + timeWindow);
            valid = false;
        }
    }

    private boolean isValidParameter(String arg) {
        return valid = parameters.containsKey(arg);
    }

    public boolean isValid() {
        return valid;
    }

    public String howTo() {
        String description = "NAME\n" +
                "       http-log-monitoring - console program that monitors HTTP traffic on running machine \n" +
                "\n" +
                "SYNOPSIS\n" +
                "\thttp-log-monitoring [OPTION]...\n" +
                "\n" +
                "DESCRIPTION\n" +
                "\tConsume an actively written-to w3c-formatted HTTP access log (/tmp/access.log by default).\n" +
                "\n" +
                "\tDisplay stats every 10 seconds about the traffic during those 10 seconds\n" +
                "\tFor instance, it displays sections of the website with most hits, the ones with less hits,\n" +
                "\tthe total number of hits and the average number of hits per section hit.\n" +
                "\n" +
                "\tDisplays an alert whenever total traffic for the past 2 minutes exceeds a certain number on average.\n" +
                "\tAlso displays an alert whenever the total traffic drops again below that value on average for the past 2 minutes.\n" +
                "\n" +
                "OPTIONS\n";

        String errorMessage = "";
        if (!errorMessages.isEmpty()) {
            errorMessage = "\n" + ANSI_RED + errorMessages.stream().map(e -> "[ERROR] " + e).collect(Collectors.joining("\n")) + ANSI_RESET + "\n\n";
        }
        return  errorMessage +
                description +
                parameters.entrySet().stream().map(e -> formatArgument(e.getKey(), e.getValue())).collect(Collectors.joining());
    }

    private String formatArgument(String argumentName, String argumentDescription) {
        return  "\t" + argumentName + "\t\t" + argumentDescription + "\n";
    }

    public int timeWindow() {
        return timeWindow;
    }

    public int threshold() {
        return threshold;
    }

    public long interval() {
        return interval * ONE_SECOND;
    }

    public String accessLogFilePath() {
        return path;
    }
}
