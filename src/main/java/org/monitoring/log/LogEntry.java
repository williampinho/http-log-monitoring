package org.monitoring.log;

/**
 * A log entry.
 */
public class LogEntry {
    private final String section;
    private final String user;
    private final long bytes;

    public LogEntry(String section, String user, long input) {
        this.section = section;
        this.user = user;
        this.bytes = input;
    }

    public String getSection() {
        return section;
    }

    public String getUser() {
        return user;
    }

    public long getBytes() {
        return bytes;
    }
}
