package org.monitoring.receiver.publisher;

import org.monitoring.log.parser.Parser;
import org.monitoring.log.parser.exception.IllegalLogFormatException;
import org.monitoring.log.queue.EventQueue;

import static org.monitoring.appender.ConsoleAppender.ANSI_RED;
import static org.monitoring.appender.ConsoleAppender.ANSI_RESET;

/**
 * Publish inputs to a queue.
 * @param <T>
 */
public abstract class Publisher<T> {
    private final EventQueue<T> queue;
    private final Parser<T> parser;

    protected Publisher(Parser<T> parser, EventQueue<T> queue) {
        this.parser = parser;
        this.queue = queue;
    }

    public void publish(String input) {
        T entry = null;

        try {
            entry = parser.parse(input);
        } catch (IllegalLogFormatException e) {
            System.out.println(ANSI_RED + "could not parse entry: " + input + ANSI_RESET);
        }

        if (entry != null) {
            queue.add(entry);
        }
    }
}
