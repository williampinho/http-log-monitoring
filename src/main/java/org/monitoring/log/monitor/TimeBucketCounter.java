package org.monitoring.log.monitor;

/**
 * Represents a counter for a specific time bucket.
 */
public class TimeBucketCounter {
    private final long timestamp;
    private int count = 1;

    public TimeBucketCounter(long timestamp) {
        this.timestamp = timestamp;
    }

    public void incrementCount() {
        count++;
    }

    public int getCount() {
        return count;
    }

    public long getTimestamp() {
        return this.timestamp;
    }
}
