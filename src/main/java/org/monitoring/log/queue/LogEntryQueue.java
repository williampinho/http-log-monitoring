package org.monitoring.log.queue;

import org.monitoring.log.LogEntry;

/**
 * Log entry queue.
 */
public class LogEntryQueue extends EventQueue<LogEntry> {
    private static LogEntryQueue INSTANCE;

    private LogEntryQueue() {
    }

    public static LogEntryQueue get() {
        if (INSTANCE == null) {
            INSTANCE = new LogEntryQueue();
        }
        return INSTANCE;
    }
}
