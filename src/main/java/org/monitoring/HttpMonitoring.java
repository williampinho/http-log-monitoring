package org.monitoring;

import org.monitoring.appender.Appender;
import org.monitoring.appender.TimedAppender;
import org.monitoring.log.LogEntry;
import org.monitoring.log.monitor.TimeWindowThresholdMonitor;
import org.monitoring.log.processor.Processor;
import org.monitoring.log.processor.StatsProcessor;

import org.monitoring.log.queue.EventQueue;
import org.monitoring.log.queue.LogEntryQueue;
import org.monitoring.log.subscriber.Subscriber;
import org.monitoring.receiver.FileReceiver;
import org.monitoring.receiver.Receiver;
import org.monitoring.receiver.publisher.AccessLogEntryPublisher;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.monitoring.appender.ConsoleAppender.ANSI_RESET;
import static org.monitoring.appender.ConsoleAppender.ANSI_YELLOW;

/**
 * Main http monitoring class. Assembles receiver, consumer and appender.
 */
public class HttpMonitoring {
    private EventQueue<LogEntry> queue = LogEntryQueue.get();
    private Appender console;
    private final long ALERT_POLLING_INTERVAL = 100;
    private final int NUMBER_THREADS = 4;
    private int timeWindow;
    private int threshold;
    private long interval;

    public HttpMonitoring withConsole(Appender console) {
        this.console = console;
        return this;
    }

    public HttpMonitoring withTimeWindow(int timeWindow) {
        this.timeWindow = timeWindow;
        return this;
    }

    public HttpMonitoring withThreshold(int threshold) {
        this.threshold = threshold;
        return this;
    }

    public HttpMonitoring withInterval(long interval) {
        this.interval = interval;
        return this;
    }

    public HttpMonitoring withAccessLogFilePath(String accessLogFilePath) {
        this.accessLogFilePath = accessLogFilePath;
        return this;
    }

    private String accessLogFilePath;

    public void start() {
        console.append(ANSI_YELLOW + "***********************************\n" +
                "\nHTTP log monitoring console program\n" +
                "\n" +
                "***********************************" + ANSI_RESET);

        Processor<LogEntry> stats = new StatsProcessor<>();
        Processor<LogEntry> monitor = new TimeWindowThresholdMonitor<>(timeWindow, threshold);

        Subscriber<LogEntry> subscriber = new Subscriber<>(queue, Arrays.asList(stats, monitor));

        TimedAppender statsAppender = new TimedAppender(console, interval, stats);
        TimedAppender monitorAppender = new TimedAppender(console, ALERT_POLLING_INTERVAL, monitor);

        Receiver<LogEntry> receiver = new FileReceiver<>(new File(accessLogFilePath).toPath(), new AccessLogEntryPublisher(queue));

        ExecutorService executor = Executors.newFixedThreadPool(NUMBER_THREADS);

        executor.submit(subscriber);
        executor.submit(statsAppender);
        executor.submit(monitorAppender);
        executor.submit(receiver);
    }
}
