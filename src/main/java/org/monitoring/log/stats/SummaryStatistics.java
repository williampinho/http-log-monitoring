package org.monitoring.log.stats;


import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.IntStream;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Generates statistics of general entries.
 */
public class SummaryStatistics {
    private final String entryName;
    private IntSummaryStatistics statistics = IntStream.of(0).summaryStatistics();
    private List<String> maxValues = emptyList();

    public SummaryStatistics(Map<String, LongAdder> entryCounter, String entryName) {
        this.entryName = entryName;
        if (!entryCounter.isEmpty()) {
            Set<Entry<String, LongAdder>> entries = entryCounter.entrySet();
            statistics = entries.stream().mapToInt(e -> e.getValue().intValue()).summaryStatistics();
            maxValues = entries.stream().filter(e -> e.getValue().intValue() == statistics.getMax()).map(Entry::getKey).collect(toList());
        }
    }

    public long getTotal() {
        return statistics.getSum();
    }

    public int getMax() {
        return statistics.getMax();
    }

    public double getAverage() {
        return statistics.getAverage();
    }

    @Override
    public String toString() {
        return  "\n\ttotal " + entryName +" hits=" + this.getTotal() +
                "\n\tmax number of hits per " + entryName + "=" + this.getMax() +
                "\n\t" + entryName + " with max hits=" + maxValues +
                "\n\taverage hits per " + entryName + "=" + this.getAverage() + "\n";
    }
}
