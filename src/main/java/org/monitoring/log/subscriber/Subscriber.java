package org.monitoring.log.subscriber;

import org.monitoring.log.processor.Processor;
import org.monitoring.log.queue.EventQueue;

import java.util.List;

/**
 * Consumes events from event queue and delegates to each processor.
 */
public class Subscriber<T> implements Runnable {

    private final EventQueue<T> queue;
    private final List<Processor<T>> processors;
    private boolean stop;

    public Subscriber(EventQueue<T> queue, List<Processor<T>> processors) {
        this.queue = queue;
        this.processors = processors;
    }

    @Override
    public void run() {
        stop = false;
        while (!stop) {
            T event = queue.poll();
            if (event != null) {
                for (Processor<T> processor : processors) {
                    processor.process(event);
                }
            }
        }
    }

    public void stop() {
        stop = true;
    }
}
