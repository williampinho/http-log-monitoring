package org.monitoring;

import org.monitoring.appender.Appender;
import org.monitoring.appender.ConsoleAppender;

import java.util.Arrays;

/**
 * Program entry point.
 */
public class Main {
    private static Appender console = new ConsoleAppender();

    public static void main(String[] args) {
        CommandLineArguments arguments = new CommandLineArguments(args);

        if (arguments.isValid()) {
            (new HttpMonitoring())
                    .withAccessLogFilePath(arguments.accessLogFilePath())
                    .withConsole(console)
                    .withInterval(arguments.interval())
                    .withThreshold(arguments.threshold())
                    .withTimeWindow(arguments.timeWindow())
                    .start();
        } else {
            console.append(arguments.howTo());
        }
    }
}
